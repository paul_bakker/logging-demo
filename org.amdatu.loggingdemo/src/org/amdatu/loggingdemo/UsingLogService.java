package org.amdatu.loggingdemo;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.osgi.service.log.LogService;

@Component
public class UsingLogService {

	@ServiceDependency
	private volatile LogService m_logService; 
	
	@Start
	public void start() {
		m_logService.log(LogService.LOG_INFO, "Logging from Log Service");
	}
}
