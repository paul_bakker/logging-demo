package org.amdatu.loggingdemo;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.Start;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

@Component
public class UsingSLF {

	Logger logger = LoggerFactory.getLogger(UsingSLF.class);
	
	@Start
	public void start() {
	    logger.info("Log with SLF4j");	
	    logger.debug("Debug log with SLF4j");
	}
}
