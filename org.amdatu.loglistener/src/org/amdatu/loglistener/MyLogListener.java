package org.amdatu.loglistener;

import org.apache.felix.dm.annotation.api.Component;
import org.apache.felix.dm.annotation.api.ServiceDependency;
import org.apache.felix.dm.annotation.api.Start;
import org.osgi.service.log.LogEntry;
import org.osgi.service.log.LogListener;
import org.osgi.service.log.LogReaderService;

@Component
public class MyLogListener {

	@ServiceDependency
	public volatile LogReaderService m_reader;
	
	@Start
	public void start() {
		m_reader.addLogListener(new MyListener());
	}
	
	class MyListener implements LogListener {
		@Override
		public void logged(LogEntry entry) {
			System.err.println("Logged entry: " + entry.getMessage());
		}
	}
}
