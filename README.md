# Requirements #

   * In our own components we use OSGi log service
   * We should be compatible with other log components, because our wrapped components use them. These logs should be readable from the Log Service
   * Components that use other log components, should be configurable on a detailed level
   * Logging should be high performing: avoid unnecessary string appends, don’t be affected by log levels which are not enabled, disabled event admin events, configure levels per bundle/service
   * Should support basic backends, such as rolling files, LogStash
   * You should be able to add your own backend easily. We strongly prefer to base this on LogReader
   * During development we need shell commands to work with logs
   * During development you should be able to configure direct output to std out/err
   * All logging config should be configurable using Config Admin


# Pax logging #
| Requirement   |      Support      | 
|----------|:-------------:|
| In our own components we use OSGi log service |  yes |
| We should be compatible with other log components, because our wrapped components use them. These logs should be readable from the Log Service |  yes |
| Components that use other log components, should be configurable on a detailed level |  Yes, using Log4j category configuration |
| Logging should be high performing: avoid unnecessary string appends, don’t be affected by log levels which are not enabled, disabled event admin events, configure levels per bundle/service | Only for SLF4j parameterized logging |
| Should support basic backends, such as rolling files, LogStash |  yes, using log4j appenders |
| You should be able to add your own backend easily. We strongly prefer to base this on LogReader |  yes, including logs written from other APIs |
| During development we need shell commands to work with logs |  - |
| During development you should be able to configure direct output to std out/err | yes, supported by log4j appender |
| All logging config should be configurable using Config Admin |  yes, using log4j properties as keys |